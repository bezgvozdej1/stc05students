package main.controllers;

import main.model.pojo.Student;
import main.services.StudentService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *
 */
public class AddStudentServlet extends HttpServlet {

    private static Logger logger = Logger.getLogger(AddStudentServlet.class);

    @Autowired
    private StudentService studentService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                config.getServletContext());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String name = req.getParameter("name");
        int age = Integer.valueOf(req.getParameter("age"));
        long groupId = Long.valueOf(req.getParameter("groupId"));
        Student student = new Student();
        student.setName(name);
        student.setAge(age);
        student.setGroupId(groupId);

        logger.debug("new name: " + name);

        long studentId = studentService.saveStudent(student);

        if (studentId > 0) {
            resp.sendRedirect(req.getContextPath() + "/students");
        }
    }
}
